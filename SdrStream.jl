module SdrStream

using DSP
#using Plots
import DSP.firfilt          # needed to be able to extend it

export firfilt, decimateN, apply, applyN, decimaten, csrc

function firfilt(b, cx)
    CHANNEL_BUFF_SIZE = 1024
    e = eltype(cx)
    function cfirfilt(outChan)
        p = 0
        nb = length(b)
        z = zeros(e, nb)
        for x in cx
            p = p + 1
            if p>nb
                p=1
            end
            z[p] = x
            acc = 0.0
            k = p
            for j = 1:nb
                acc = acc + b[j]*z[k]
                k = k - 1
                if k < 1
                    k = nb
                end
            end
            put!(outChan, acc)
        end
    end
    return Channel(cfirfilt, ctype=e, csize=CHANNEL_BUFF_SIZE)
end



function decimateN(n, cx)
    CHANNEL_BUFF_SIZE = 1024
    n::Int64
    e = eltype(cx)
    function cdecimateN(outChan)
        for (i, x) in enumerate(cx)
            if i % n == 0
                put!(outChan, x)
            end
        end
    end
    return Channel(cdecimateN, ctype=e, csize=CHANNEL_BUFF_SIZE)
end



function apply(f, cx)
    CHANNEL_BUFF_SIZE = 1024
    e = eltype(cx)
    function capply(outChan)
        for x in cx
            put!(outChan, f(x))
        end
    end
    return Channel(capply, ctype=e, csize=CHANNEL_BUFF_SIZE)
end



function applyN(n, f, cx)
    CHANNEL_BUFF_SIZE = 1024
    n::Int
    e = eltype(cx)
    function capplyN(outChan)
        s = zeros(n)
        i = 0
        for x in cx
            i += 1
            s[i] = x
            if i == n
                put!(outChan, f(s...))
                i = 0
            end
        end
    end
    return Channel(capplyN, ctype=e, csize=CHANNEL_BUFF_SIZE)
end


# Use applyN to define the decimateN function in another way
decimaten(n, xc) = applyN(n, (x1, x2, x3)->x3, xc)



# From array to Channel
function csrc(arr)
    CHANNEL_BUFF_SIZE = 1024
    e = eltype(arr)
    return Channel(c->foreach(i->put!(c, i), arr), ctype=e, csize=CHANNEL_BUFF_SIZE)
end

end

